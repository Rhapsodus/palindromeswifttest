import UIKit

class ViewController: UIViewController
{
    //******************************************************************************
    // Learning Swift:
    //     isPalindrome returns true if the string is reflective about its length/2
    // Goal is to manipulate strings in an efficient manner
    //******************************************************************************
    private func isPalindrome( inout testString : String ) -> Bool
    {
        let letters = Array(testString.characters)
        let len = testString.characters.count;
        let halfLen = len>>1;
        for i in 0..<halfLen // 21 seconds
        {
            if ( letters[i] != letters[len-i-1])
            {
                return false
            }
        }
        
        return true
    }
    
    //******************************************************************************
    // View loaded
    //******************************************************************************
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //******************************************************************************
        // Performance test, lets test 1 million calls to isPalindrome
        //******************************************************************************
        var palindrome = "n1everoddoreven";
        var count = 0;
        let start = NSDate();
        for _ in 0..<1000000
        {
            if( isPalindrome(&palindrome) )
            {
                count += 1;
            }
        }
        let end = NSDate();
        let timeInterval: Double = end.timeIntervalSinceDate(start);
        print("Time: \(timeInterval) seconds");
    }
}

